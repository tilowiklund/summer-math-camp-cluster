#! /bin/bash

# Fail on first error
set -e

if [ $# -ne 2 ]; then
	echo "USAGE: ${0} THIS_ID TOTAL_NUMBER_OF_NODES"
	exit 1
fi

ID=$1
NUM_MACHINES=$2

hostnamectl set-hostname squid-$ID

# Install latest version of docker
apt-get update
apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

# Install latest version of docker-compose
apt-get install -y python-pip
pip install docker-compose

# Add user to docker group
usermod -a -G docker squid

# Install OpenSSH server and get keys
apt-get install -y openssh-server
mkdir -p /home/squid/.ssh
cp squid_key squid_key.pub config /home/squid/.ssh/
cp squid_key.pub /home/squid/.ssh/authorized_keys
chown -R squid:squid /home/squid/.ssh

# Install ansible
apt-get install -y ansible

# Set wired ip and enable interface on boot
nmcli connection modify Wired\ connection\ 1 ipv4.addresses 10.0.10.10$ID/24 ipv4.method manual
echo "auto eno1" >> /etc/network/interfaces

# Add machines to hosts file
for i in $(seq 1 $NUM_MACHINES); do
	if [ $i -ne $ID ] && ! grep -Fxq "10.0.10.10$i squid-$i" /etc/hosts; then
		echo "10.0.10.10$i squid-$i" >> /etc/hosts
	fi
done

# Write ansible hosts file
if ! grep -Fxq "[others]" /etc/ansible/hosts; then
	echo "squid-$ID" >> /etc/ansible/hosts
	echo "[others]" >> /etc/ansible/hosts
	for i in $(seq 1 $NUM_MACHINES); do
		if [ $i -ne $ID ]; then
			echo "squid-$i" >> /etc/ansible/hosts
		fi
	done
fi
